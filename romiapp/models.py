from django.db import models

# Create your models here.
from django.db import models
from django.utils import timezone

class Schedule(models.Model):
    nama_kegiatan = models.CharField(max_length=50)
    nama_tempat = models.CharField(max_length=50)
    kategori_kegiatan = models.CharField(max_length=50)
    waktu_kegiatan = models.DateTimeField() 
