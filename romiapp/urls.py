from django.conf.urls import url
from .views import *
urlpatterns = [
    url(r'^$', home, name='home'),
    
    url(r'^create_schedule', schedule, name='create_schedule'),
    url('schedule', schedule, name='schedule'),
    url("display", display, name="display"),
    url("form_page", form_page, name="form_page"),
    url("afterpurge", purge, name="purge"),
    
    
    
]
