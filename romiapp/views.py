from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Schedule
from .forms import Project_Form

# Create your views here.

def home(request):
    response={}
    return render(request, 'home.html', response)

def form_page(request):
        response={}
        response['message_form'] = Project_Form
        return render(request, "form_page.html", response)

def schedule(request):
    response={}
    print("hai")
    form = Project_Form(request.POST or None)
    if(request.method == 'POST'):
            response['nama_kegiatan'] = request.POST['nama_kegiatan']
            response['nama_tempat'] = request.POST['kategori_kegiatan']
            response['waktu_kegiatan'] = request.POST['waktu_kegiatan']
            response['kategori_kegiatan']= request.POST['kategori_kegiatan']
            
            schedule = Schedule(nama_kegiatan=response['nama_kegiatan'], nama_tempat=response['nama_tempat'], waktu_kegiatan=response['waktu_kegiatan'], kategori_kegiatan=response['kategori_kegiatan'])
            print("heloix")
            schedule.save()

            html='display.html'
            return render(request, html, response)
    else:
           return HttpResponseRedirect('/')

def display(request):
    schedules = Schedule.objects.all()
    response = { "Schedules" : schedules }
    return render(request, 'display.html' , response)


def purge(request):
    Schedule.objects.all().delete()
    return HttpResponseRedirect('display')





