from django import forms

class Project_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }
    nama_kegiatan = forms.CharField(max_length=30, widget=forms.TextInput(attrs=attrs))
    nama_tempat = forms.CharField(max_length=30, widget=forms.TextInput(attrs=attrs))
    waktu_kegiatan = forms.DateTimeField(widget=forms.TextInput(attrs={'type':'datetime-local', 'class':'form-control'}))
    kategori_kegiatan = forms.CharField(max_length=30, widget=forms.TextInput(attrs=attrs))
