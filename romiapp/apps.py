from django.apps import AppConfig


class RomiappConfig(AppConfig):
    name = 'romiapp'
